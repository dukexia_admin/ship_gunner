#!/usr/bin/env python3
# 模拟炮舰主炮设计游戏


import pygame
from pygame.locals import *
from sys import exit
from threading import Thread,Lock,Timer
import time,math
from random import random
from lib.my_locale import msg

#273 上
#274 下
#275 左
#276 右
#32  空格
#27  ESC

LEFT_DOWN=False
RIGHT_DOWN=False
LOCK1 = Lock()
is_alive = True

def update_screen():
    global is_alive,LOCK1
    clock1 = pygame.time.Clock()
    cycle = 0
    ship1_event = pygame.event.Event(USEREVENT, message="h")
    while is_alive:
        try:
            LOCK1.acquire()
            pygame.display.flip()
            if cycle%25==0:
                pygame.event.post(ship1_event)
            cycle += 1
            LOCK1.release()
        except:
            pass
        clock1.tick(60)
        
background_image_filename = 'pics/bg2.png'
ship_image_filename = 'pics/ship1.png'
hole_img_filename = 'pics/hole1.png'
 
pygame.init()
SCREEN_SIZE = (800, 600)
Fullscreen = True
DEBUG=0  # 0、1 切换全屏、窗口模式
if DEBUG==1:
    screen = pygame.display.set_mode(SCREEN_SIZE, 0, 32)
    Fullscreen = False
else:
    screen = pygame.display.set_mode(SCREEN_SIZE, FULLSCREEN|DOUBLEBUF, 32)
pygame.mixer.init()

background = pygame.image.load(background_image_filename).convert()
ship1 = pygame.image.load(ship_image_filename)
hole1 = pygame.image.load(hole_img_filename)

font = pygame.font.Font("hei1.ttf", 18);
font_height = font.get_linesize()

pygame.event.set_blocked(None)
pygame.event.set_allowed( [MOUSEBUTTONUP,KEYDOWN,KEYUP,QUIT,USEREVENT] )

message = ''
x = 358
y = 273
target = [x+50,y+15]
gun_aim = (386,291)
hit_dist = (0,0)

def verify_hit(effect_hit_dist,effect_wucha):
    global target,gun_aim,hit_dist
    hit_wucha = (effect_wucha[0]*(random()-0.5)*2,effect_wucha[1]*(random()-0.5)*2)
    hit_dist = (gun_aim[0]-target[0]+hit_wucha[0],gun_aim[1]-target[1]+hit_wucha[1])
    if hit_dist[0]*hit_dist[0] <= effect_hit_dist[0]*effect_hit_dist[0] and \
       hit_dist[1]*hit_dist[1] <= effect_hit_dist[1]*effect_hit_dist[1]:
        return True
    else:
        return False
        
hit_count = 0
hit_report = ''
damage = 0

def hit_object( smoke_point,x,y ):
    global hit_report,hit_count,damage,LOCK1
    LOCK1.acquire()
    smoke_point.append( (x,y) )
    damage += 50 - math.sqrt(x*x)
    hit_count += 1
    if damage<100:
        hit_report=msg['hit'] 
    else:
        hit_report=msg['damage']
    LOCK1.release()

def hit_missed():
    global hit_report,LOCK1
    LOCK1.acquire()
    hit_report = msg['miss']
    LOCK1.release()

def damage_ship(screen,x,y):
    global LOCK1
    LOCK1.acquire()
    screen.blit( font.render( msg['long_damage'] ,\
                              False, (255, 0, 0)), (320, 200) )
    ship2 = pygame.image.load('pics/baozha1.png')
    screen.blit(ship2,(x,y))
    screen.blit( font.render(msg['damage'], False, (255, 0, 0)), (10, font_height) )
    pygame.display.flip()
    
    bs = pygame.mixer.Sound('sound/bomb.wav')
    bs.set_volume(0.5)
    bs.play()
    time.sleep(1)
    ship2 = pygame.image.load('pics/baozha2.png')
    screen.blit(ship2,(x,y))
    pygame.display.flip()
    time.sleep(1)
    ship2 = pygame.image.load('pics/baozha3.png')
    screen.blit(ship2,(x,y))
    pygame.display.flip()
    LOCK1.release()
    time.sleep(1)

def shot_main( distance=4000 ):
    global Fullscreen,x,y,target,gun_aim,LEFT_DOWN,RIGHT_DOWN,hit_dist,hit_report,damage,\
           font_height,background,screen,font,ship1,hole1,message,is_alive,hit_count
    is_alive=True
    t_screen= Thread(target=update_screen)
    t_screen.start()
    LEFT_DOWN=False
    RIGHT_DOWN=False
    start_time = time.time()
    damage = 0
    hit_count=0
    hit_report=''
    dy=0  #垂直射角调整量
    message = ''
    x = 358
    y = 273
    target = [x+50,y+15]
    gun_aim = (386,291)
    hit_dist = (0,0)
    smoke_point = []
    base_hit_dist = (45,12)
    effect_hit_dist = (base_hit_dist[0]*2000.0/distance,base_hit_dist[1]*2000.0/distance)
    base_wucha = (1.0,1.0)
    effect_wucha = (base_wucha[0]*distance/2000.0,base_wucha[1]*distance/2000.0,)
    hit_time = distance/1000.0
    fire_count = 0
    bt_win = (330,568,40,30)
    bt_quit = (390,568,40,30)
    fire = pygame.mixer.Sound(file='sound/fire.wav')
    load_timestamp = time.time()-6
    mission_time = time.time()
    while True:
        event = pygame.event.wait()
        d_time = time.time()-start_time
        d_y = int(math.sin( math.pi*d_time/6.0)*30)
        target = [x+50,y+d_y+dy+15]
        if event.type == QUIT:
            is_alive = False
            break
            
        LOCK1.acquire()
        if event.type == KEYDOWN:
            if event.key == 273:
                dy += 1
                target[1] += 1
            elif event.key == 274:
                dy -= 1
                target[1] -=1
            elif event.key == 275:
                RIGHT_DOWN = True
            elif event.key == 276:
                LEFT_DOWN = True
            elif event.key == 32:
                if time.time()-load_timestamp>5:
                    fire.play()
                    if verify_hit(effect_hit_dist,effect_wucha):
                        timer1 = Timer(hit_time,hit_object,args=(smoke_point,hit_dist[0]*distance/2000.0,10 ))
                        timer1.start()
                    else:
                        timer2 = Timer(hit_time,hit_missed)
                        timer2.start()
                    fire_count += 1
                    load_timestamp = time.time()
            elif event.key == 27:
                is_alive = False
                LOCK1.release()
                break
        elif event.type == KEYUP:
            if event.key == 275:
                RIGHT_DOWN = False
            elif event.key == 276:
                LEFT_DOWN = False
        elif event.type == USEREVENT:
            if event.message=='h':
                x -= 1
                if LEFT_DOWN:
                    x+=3
                elif RIGHT_DOWN:
                    x-=2
        elif event.type == MOUSEBUTTONUP:
            if isOver(bt_win):
                Fullscreen = not Fullscreen
                if Fullscreen:
                    screen = pygame.display.set_mode(SCREEN_SIZE, FULLSCREEN|DOUBLEBUF, 32)
                else:
                    screen = pygame.display.set_mode(SCREEN_SIZE, 0, 32)
            if isOver(bt_quit):
                is_alive = False
                LOCK1.release()
                break
        if time.time()-load_timestamp>5:
            message = msg['ready_fire']
        else:
            message = msg['loading']
        screen.fill((255, 255, 255))
        screen.blit(background, (0,d_y+dy))
        screen.blit(ship1,(x,y+d_y+dy))
        screen.blit(hole1,(0,0))
        for p in smoke_point:
            show_smoke(screen,target[0]+p[0],target[1]+p[1])
        pygame.draw.circle( screen,(255,0,0),gun_aim,2 )
        screen.blit( font.render(msg['count_fire'].format(fire_count), False, (0, 255, 0)), (10, 0) )
        screen.blit( font.render(msg['count_hit'].format(hit_count,damage), False, (0, 255, 0)), (200, 0) )
        screen.blit( font.render( hit_report, False, (255, 0, 0)), (10, font_height) )
        screen.blit( font.render( message, False, (255, 0, 0)), (300, font_height) )
        screen.blit( font.render(msg['distance'] .format(distance),\
                      False, (0, 255, 0)), (600, 0) )
        if Fullscreen:
            win_mode = msg['win']
        else:
            win_mode = msg['fs']
        draw_text_button(screen,bt_win,win_mode)
        draw_text_button(screen,bt_quit,msg['quit'])
        LOCK1.release()
        if damage>=100:
            is_alive = False
            time.sleep(1)
            damage_ship(screen,x,y+d_y+dy)
            break
        p_x = target[0]-gun_aim[0]
        if p_x*p_x>40000 or time.time()-mission_time >600:
            screen.blit( font.render( msg['fail'], False, (255, 0, 0)),  (320, 200)  )
            pygame.display.flip()
            time.sleep(2)
            is_alive = False
            break
        
    
smoke1_filename = 'pics/smoke1.png'
smoke1 = pygame.image.load(smoke1_filename)

def show_smoke(screen,x,y):
    global smoke1
    screen.blit(smoke1,(x-10,y-60))

def ask_todo(dist):
    global screen
    screen.fill((0,0,0))
    screen.blit( font.render( msg['success'].format(dist),\
                              False, (255, 0, 0)), (270, 150) )
    bt1 = (300,350,170,30)
    bt2 = (300,200,170,30) #dist-1000
    bt3 = (300,250,170,30) #dist
    bt4 = (300,300,170,30) #dist+1000
    draw_text_button(screen,bt1,msg['quit'])
    draw_text_button(screen,bt2,msg['fire_dist'].format(dist-1000))
    draw_text_button(screen,bt3,msg['fire_dist'].format(dist))
    draw_text_button(screen,bt4,msg['fire_dist'].format(dist+1000))
    pygame.display.flip()
    ret = True
    while True:
        event = pygame.event.wait()
        if event.type == MOUSEBUTTONUP:
            if isOver(bt1):
                ret = 0
                break
            elif isOver(bt2):
                ret = dist-1000
                break
            elif isOver(bt3):
                ret = dist
                break
            elif isOver(bt4):
                ret = dist+1000
                break
    return ret

def draw_text_button(screen,rect=(10,10,10,10),text='b'):
    pygame.draw.rect(screen,(0,255,0),rect,2)
    screen.blit( font.render( text, False, (0, 255, 0)), (rect[0]+3, rect[1]+5) )

def isOver(rect=(0,0,1,1)):
    point_x,point_y = pygame.mouse.get_pos()
    in_x = rect[0] < point_x <rect[0]+rect[2]
    in_y = rect[1] < point_y <rect[1]+rect[3]
    return in_x and in_y
    
if __name__=='__main__':
    dist=4000
    while True:
        shot_main(dist)
        dist = ask_todo(dist)
        if dist == 0:
            break
